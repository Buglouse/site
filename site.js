function submitKeys(e) {
    if(e && e.keyCode == 13) {
        document.forms[0].submit();}}
function submitJSON() {
    // onsubmit="submitJSON()" enctype="multipart/form-data""
    var $f = $('#data');
    var $m = $('#message');
    $.ajax({
        type: 'POST',
        url: $f.attr('action')+'&amp;JSON=1',
        data: $f.serialize(),
        success: function(msg) {
            var formResponse = eval(msg);
            if (formResponse.FormProcessV2Response.success) {
                $m.addClass('success').fadeIn().html(formResponse.FormProcessV2Response.message);
                $f.fadeOut();
            }},
        error: function(msg) {
            alert('error'+msg);
            return false;}});}

(lambda (path)
  ;;(nav `(div (@ (class "menu"))
  ;; same file
  (define doc-dir (or (get-environment-variable "SITE_DIR_DOCS")
                      (conc (get-environment-variable "HOME") "/data")))
  (define doc-navs `(("Employment" "resume")))
  (define index
    `((h "Index")
      (div (@ (class "menu"))
           (ul
            ,(map (lambda (n)
                    `(li (a (@ (href ,(conc "/doc/" (cadr n)))) ,(car n))))
                  doc-navs)))))

  (cond
   ((string-match "/doc/?" path)
    index)
   ((string-match "/doc/log" path)
    (map (lambda (e)
           `(a (@ (href ,(pathname-file e))) ,(pathname-file e)))
         (glob "./doc/log/*.sxml")))
   ((string-match ".*\\.(pdf|html)" path)
    (redirect-to (conc "file://" doc-dir path)))
   (else
    (let ((file (conc doc-dir path ".sxml")))
      (if (and (file-exists? file)
               (file-read-access? file))
          (eval (with-input-from-file file read))
          (and (alerts (conc "Not Found(doc): " (default-host) "/" file))
               index))))))

#!/usr/bin/env
#|
exec csi -s "${0}" "${@}"
|#
(use spiffy awful)
(include "site.scm")
;; Spiffy
(server-port 8081)
;;(spiffy-user "web")
;;(spiffy-group "web")

(awful-apps '("site.scm"))
(awful-start
 (lambda ()
   (reload-apps (awful-apps))
   (fprintf (current-error-port)
            "~S:~S~%" (server-bind-address) (server-port))))

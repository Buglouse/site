(lambda (path)
  ;; ID,URI,TXT,TAG
  (module mark
      *
    (import scheme chicken)
    (use data-structures)
    (use db)

    (define mark-table 'mark)
    (define mark-db '((mark ("id INTEGER PRIMARY KEY"
                             "uri TEXT NOT NULL COLLATE NOCASE"
                             "txt TEXT"
                             "tag TEXT"
                             "UNIQUE(uri)"))))
    (define mark-path (conc (get-environment-variable "HOME") "/data/doc/" "mark.db"))

    (db-table mark-db)
    (db-path mark-path)
    (or (file-exists? (db-path)) (init))

    (define (init)
      (db-create mark-table))
    (define (insert kv)
      (db-insert mark-table kv))
    (define (view #!optional kv)
      (db-select mark-table kv))
    (define (update k v)
      (db-update mark-table (string->number k) v))
    (define (delete k)
      (db-delete mark-table (string->number k)))

    )
  (import mark)
  (with-request-variables (uri txt tag id action)
                          (and action
                               (cond
                                ((string=? action "Update")
                                 (update id (list uri txt tag)))
                                ((string=? action "Submit")
                                 (insert (list uri txt tag)))
                                ((string=? action "Delete")
                                 (delete id)))))
  (let ((doc-path (conc (get-environment-variable "HOME") "/data/doc/"))
        (marks (view))
        (uri ($ 'uri))
        (tag ($ 'tag))
        (txt ($ 'txt))
        (edit ($ 'edit))
        (xbel ($ 'xbel)))
    (define (f-insert)
      `(form (@ (name entry) (method post))
             (label (@ (for uri)) "URI")
             (input (@ (type url) (name uri) (value ,(and edit uri)) (placeholder "http://")))
             (label (@ (for txt)) "Txt")
             (input (@ (type text) (name txt) (value ,(and edit txt)) (placeholder "Title")))
             (label (@ (for tag)) "Tag")
             (input (@ (type text) (name tag) (value ,(and edit tag)) (placeholder "tag;tags")))
             ,(if edit
                  `((input (@ (type submit) (name action) (value "Update")))
                    (input (@ (type submit) (name action) (value "Delete"))))
                  `(input (@ (type submit) (name action) (value "Submit"))))
             (input (@ (type hidden) (name id) (value ,(and edit))))
             ))
    (define (xbel)
      ;; Export to XBEL
      ;;<?xml version="1.0" encoding="UTF-8"?>
      ;;<!DOCTYPE xbel PUBLIC "+//IDN python.org//DTD XML Bookmark Exchange Language 1.0//EN//XML"
      ;;"http://www.python.org/topics/xml/dtds/xbel-1.0.dtd">
      `(xbel ,(map (lambda (e)
                     (let ((id (cdr (assoc 'id e)))
                           (uri (cdr (assoc 'uri e)))
                           (txt (cdr (assoc 'txt e)))
                           (tag (cdr (assoc 'tag e))))
                       `(bookmark (@ (href ,uri))
                                  (title ,txt)
                                  (tag ,tag))))
                   marks)))
    (define-page "mark/mark.xbel"
      (lambda ()
        (conc
         "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" "\n"
         "<!DOCTYPE xbel PUBLIC \"+//IDN python.org//DTD XML Bookmark Exchange Language 1.0//EN//XML\" \"http://www.python.org/topics/xml/dtds/xbel-1.0.dtd\">"
         ((sxml->html) (xbel))))
      no-template: #t
      use-sxml: #f)
    `((style ,(scss->css `(css+
                           ((= id marks)
                            (display grid)
                            (grid-auto-columns 5% 10% 10%)
                            (margin 0 auto 0 auto)
                            (width 50%)
                            ((// input)
                             (margin-left -5em)
                             (margin-right 1em)
                             (grid-column 1))
                            ((// a)
                             (grid-column 2))
                            ((// button)
                             (width 200%)
                             (margin-left 3em)
                             (grid-column 3))
                            ))))
      ,(f-insert) (br)
      ,(map (lambda (e)
              (let ((id (cdr (assoc 'id e)))
                    (uri (cdr (assoc 'uri e)))
                    (txt (cdr (assoc 'txt e)))
                    (tag (cdr (assoc 'tag e))))
                ;; Align print
                (if (< id 10) (set! id (conc 0 id)))
                `((form (@ (name entry) (method post) (id marks))
                        (input (@ (type submit) (name edit) (value ,id) (class id)))
                        (input (@ (type hidden) (name uri) (value ,uri)))
                        (input (@ (type hidden) (name txt) (value ,txt)))
                        (input (@ (type hidden) (name tag) (value ,tag)))
                        (a (@ (href ,uri) (class uri))
                           ,txt)
                        ,(map (lambda (ee)
                                `(button (@ (type submit) (name tag) (value ,ee) (class tag)) ,ee))
                              (string-split tag ";")))
                  (br))))
            marks)
      (a (@ (href "mark/mark.xbel")) "XBEL"))
    ))

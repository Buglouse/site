(import scheme chicken)
(use data-structures files extras)
(use spiffy awful doctype html-utils regex posix sxml-transforms scss intarweb uri-common)

(define page-title (make-parameter "Title"))
(define page-head (make-parameter "/head.sxml"))
(define page-foot (make-parameter "/foot.sxml"))
(define page-dev (make-parameter "/dev.sxml"))
(define file-scss (make-parameter "site.scss"))
(define file-jscript (make-parameter "site.js"))
(define w-404 "Not Found (404)")
(define alerts (make-parameter ""))
(define navs '(("Resume" "doc/resume")
               ("Mark" "mark")))
(define (css-theme #!optional theme)
  (let* ((w-theme "Invalid CSS Theme")
         (w-theme-key "Invalid CSS Theme key")

         (white "#ffffff")
         (silver "#c0c0c0")
         (gray "#808080")
         (black "#000000")
         (red "#ff0000")
         (maroon "#800000")
         (yellow "#ffff00")
         (olive "#808000")
         (lime "#00ff00")
         (green "#008000")
         (aqua "#00ffff")
         (teal "008080")
         (blue "#0000ff")
         (navy "#000080")
         (fuchsia "#ff00ff")

         (grey1 "#59443C")
         (grey2 "#8C8281")
         (grey3 "#EDECEB")
         (red1 "#B64926")
         (red2 "#9C4320")
         ;; "Roboto Helvetica"
         (font "Verdana, Arial, sans-serif")
         (background white)
         (foreground "#4e443c")
         (nav-background cyan)
         (link red)
         (link-hover-foreground "#F90F90")
         (link-hover-background "#FFFFFF")
         (link-shadow "#333333"))
    (define (get key)
      (let ((theme (or (and (list? theme) theme) (eval theme))))
        (or theme (list? theme) (error 'scss w-theme theme))
        (let ((val (or (alist-ref key theme)
                       (alist-ref key default))))
          (if val (car val) (and (warning w-theme-key key) "")))))))

;;WTF?
(handle-not-found (lambda (page) "Not found"))

;; Awful
(app-root-path "/")
(main-page-path "/")
(login-page-path "")
(page-doctype doctype-html)
(page-charset "utf-8")
(enable-sxml 't)
;;(development-mode? 't)
;;jk(sxml->html )
(page-access-denied-message (lambda (path)
                              "Denied"))
(page-exception-message
 (lambda (exn)
   `(pre ,(with-output-to-string
            (lambda ()
              (print-call-chain)
              (print-error-message exn))))))
(sxml->html
 (let ((rules
        `((literal *preorder* . ,(lambda (t b) b))
          ;;(pp *macro* . ,(lambda (t b) `(p ,(intersperse b '(br)))))
          ;;(string->goodHTML (->string body))
          ;;(ln *macro* . ,(lambda (t b) b))
          ;;(frag *macro* . ,(lambda (t b) `(h2 ,b)))
          (h *macro* . ,(lambda (t b) `(h2 ,b)))
          ;;(e *macro* . ,(lambda (t b) `(h ,b)))
          ;;(l *macro* . ,(lambda (t b) `(li ,b)))
          ;;(date *macro* . ,(lambda (t b) `(p "Date: " ,b)))
          . ,universal-conversion-rules*)))
   (lambda (sxml)
     (with-output-to-string
       (lambda ()
         (SRV:send-reply (pre-post-order* sxml rules)))))))
(page-template
 (lambda (content . rest)
   (let ((meta-default
          '((meta (@ (name "viewport")
                     (content "width=device-width,initial-scale=1,minimum-scale=0.25,maximum-scale=4,user-scalable=yes")))
            (meta (@ (name "description")
                     (content "Web")))
            (meta (@ (name "robots")
                     (content "index,follow")))
            (meta (@ (nane "author")
                     (content "Public")))
            (meta (@ (name "copyright")
                     (content "share alike")))
            (meta (@ (name "keywords")
                     (content "web")))
            (link (@ (rel "manifest")
                     (href "manifest.json")))))
         (meta-ie
          '((meta (@ (http-equiv "X-UA-Compatible")
                     (content "IE=edge,chrome=1")))
            (meta (@ (name "msapplication-tap-highlight")
                     (content "no")))))
         (meta-ms
          '((meta (@ (name "msapplication-TileImage")
                     (href "")))
            (meta (@ (name "msapplication-TileColor")
                     (content "#2F3BA2")))))
         (meta-android-chrome
          '((meta (@ (name "mobile-web-app-capable")
                     (content "yes")))
            (meta (@ (name "application-name")
                     (content "title")))
            (link (@ ((rel "icon")
                      (href ""))))))
         (meta-ios-safari
          '((meta (@ (name "apple-mobile-web-app-capable")
                     (content "yes")))
            (meta (@ (name "apple-mobile-web-app-status-bar-style")
                     (content "black")))
            (meta (@ (name "apple-mobile-web-app-title")
                     (content "Web Starter Kit")))
            (link (@ (rel "apple-touch-icon")
                     (href "")))))
         (meta-mobile
          '((meta (@ (name "theme-color")
                     (content "#2F3BA2")))
            (link (@ (rel "canonical")
                     (href "http://mobile.example.net/">)))
            (meta (@ (name "HandheldFriendly")
                     (content "true")))))
         (attr-en
          '((lang "en")
            (xml:lang "en")
            (xmlns "http://www.w3.org/1999/xhtml")))
         (meta-css
          ;;`(link (@ (rel "stylesheet") (type "text/css") (href ,file-css)))
          `(style ,(eval (with-input-from-file (file-scss) read))))
         (meta-jscript
          `(script (@ (type "text/javascript"
                            (src ,file-jscript))))))

     ;;(fprintf (current-error-port) "~A~%~A~%" (request+response))
     ;;(display (request+response) (current-error-port))
     ;;(awful-response-headers '((content-type "text/json")))
     (html-page
      `(
                                        ;(header ,(eval (with-input-from-file (conc (root-path) (page-head)) read)))
        ;;(div (@ (class clear)))
                                        ;(aside (@ (id alert)) ,(alerts)) (br)
        ;;(div (@ (class clear)))
        (main ,content)
        ;;(div (@ (class clear)))
                                        ;(footer ,(eval (with-input-from-file (conc (root-path) (page-foot)) read)))
                                        ;(hr)
                                        ;(hr)
        ;;(div (@ (class clear)))
        ;;(section (@ (id "development")) ,(eval (with-input-from-file (conc (root-path) (page-dev)) read)))
        )
      ;;title: (page-title)
      doctype: (page-doctype)
      charset: (page-charset)
      html-attribs: attr-en
      sxml?: 't
      headers: (list
                meta-default
                meta-ie
                meta-ms
                meta-android-chrome
                meta-ios-safari
                meta-mobile
                meta-css
                ;;meta-jscript
                )
      rest))))

(define-app site
  matcher: (lambda (path)
             (string-prefix? "/" path))

  (define-page "/r"
    (lambda ()
      (reload-apps (awful-apps))
      `((p ,(seconds->string (current-seconds)))
        (ul ,@(map (lambda (app)
                     `(li (code ,app)))
                   (awful-apps)))))
    title: "Reload")
  (define-page "/dev"
    (lambda ()
      (dev)))
  #;(define-page (regexp ".*\\.[a-z]{3,4}")
  (lambda (page)
  ;;(redirect-to (conc "file:///" (root-path) page))))
  page))

  (define-page "/"
    (lambda ()
      (eval (with-input-from-file "index.sxml" read))))

  (define-page (regexp "/doc/?.*")
    (lambda (path)
      ((eval (with-input-from-file "doc.scm" read)) path)))
  (define-page (regexp "/mark/?.*")
    (lambda (path)
      ((eval (with-input-from-file "mark.scm" read)) path))
    method: '(GET POST))

  (define-page (regexp "/.*")
    (lambda (page)
      (conc "Not Found: " page)
      ))
  )
